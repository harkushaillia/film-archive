from flask import current_app
from flask_httpauth import HTTPTokenAuth, HTTPBasicAuth
from werkzeug.exceptions import Forbidden, Unauthorized

from app import db
from app.models import User

basic_auth = HTTPBasicAuth()
token_auth = HTTPTokenAuth()


@basic_auth.verify_password
def verify_password(username: str, password: str) -> User:
    if username and password:
        user = User.query.filter_by(username=username).first()
        if user and user.check_password(password):
            return user


@basic_auth.error_handler
def basic_auth_error(status: int = 401) -> (dict, int, dict):
    error = (Forbidden if status == 403 else Unauthorized)()
    return {
               'code': error.code,
               'name': error.name,
               'description': error.description,
           }, error.code, {'WWW-Authenticate': 'Form'}


@token_auth.verify_token
def verify_token(access_token: str) -> User:
    if current_app.config['DISABLE_AUTH']:
        return db.session.get(User, 1)
    if access_token:
        return User.check_access_token(access_token)


@token_auth.error_handler
def token_auth_error(status: int = 401) -> (dict, int):
    error = (Forbidden if status == 403 else Unauthorized)()
    return {
               'code': error.code,
               'name': error.name,
               'description': error.description,
           }, error.code
