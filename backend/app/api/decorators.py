from functools import wraps
from typing import Callable

from apifairy import arguments, response
from flask import current_app, abort
from sqlalchemy import extract

from app.api.schemas import PaginationSchema, paginated_collection


def paginated_response(schema, page_default=1,
                       pagination_schema=PaginationSchema) -> Callable:
    def inner(f: Callable) -> Callable:
        @wraps(f)
        def paginate(*args, **kwargs) -> dict:
            args = list(args)
            pagination = args.pop(-1)
            t_query = f(*args, **kwargs)

            # filters
            if (year := pagination.get('year', None)) is not None:
                t_query = t_query.filter(extract(
                    'year', t_query.column_descriptions[0]['expr'].release_date)
                                         == year)
            if (genre := pagination.get('genre', None)) is not None:
                t_query = t_query.filter(t_query.column_descriptions[0]['expr']
                                         .genres.any(id=genre))
            if (director := pagination.get('director', None)) is not None:
                t_query = t_query.filter_by(director_id=director)

            # sorting
            if (sort_by := pagination.get('sort_by', None)) is not None:
                t_query = t_query.order_by(sort_by)

            # pagination
            count = t_query.count()
            limit = pagination.get(
                'limit', current_app.config['ITEMS_PER_PAGE'])
            page = pagination.get('page', page_default)
            offset = (page - 1) * limit
            if page <= 0 or (0 < count <= offset) or limit <= 0:
                abort(404)

            query = t_query.limit(limit).offset(offset)
            data = query.all()
            return {'data': data, 'pagination': {
                'page': page,
                'limit': limit,
                'count': len(data),
                'total': count
            }}

        return arguments(pagination_schema)(response(paginated_collection(
            schema, pagination_schema=pagination_schema))(paginate))

    return inner
