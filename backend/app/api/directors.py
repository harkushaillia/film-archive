from apifairy import body, response, authenticate, other_responses
from flask import Blueprint, abort, current_app

from app import db
from app.api.auth import token_auth
from app.api.schemas import DirectorSchema
from app.models import Director

directors = Blueprint('directors', __name__)
director_schema = DirectorSchema()
directors_schema = DirectorSchema(many=True)


@directors.route('/directors', methods=['POST'])
@body(director_schema)
@authenticate(token_auth)
@response(director_schema, 201)
@other_responses({403: 'Only admins are allowed to register a new director'})
def new(args) -> Director:
    """Register a new director"""
    if not token_auth.current_user().is_admin:
        abort(403)
    director = Director(**args)
    db.session.add(director)
    db.session.commit()
    current_app.logger.info(
        f'Director {director.name} was added successfully')
    return director


@directors.route('/directors', methods=['GET'])
@response(directors_schema)
def get_all() -> list:
    """Get all directors"""
    return Director.query.all()


@directors.route('/directors/<int:director_id>', methods=['GET'])
@response(director_schema)
def get(director_id: int) -> Director:
    """Get the director by id"""
    return Director.query.get(director_id) or abort(404)


@directors.route('/directors/<int:director_id>', methods=['DELETE'])
@authenticate(token_auth)
@other_responses({403: 'Only admins are allowed to delete the directors'})
def delete(director_id: int) -> (str, int):
    """Delete the director"""
    director = Director.query.get(director_id) or abort(404)
    if not token_auth.current_user().is_admin:
        abort(403)
    db.session.delete(director)
    db.session.commit()
    current_app.logger.info(f'Director {director.name} was removed')
    return '', 204
