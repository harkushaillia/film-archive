from flask import Blueprint, current_app
from sqlalchemy.exc import IntegrityError, SQLAlchemyError
from werkzeug.exceptions import HTTPException, InternalServerError

from app import apifairy

errors = Blueprint('api_errors', __name__)


@errors.app_errorhandler(HTTPException)
def http_error(error):
    current_app.logger.error(f'{error.code} {error.name} {error.description}')
    return {
               'code': error.code,
               'message': error.name,
               'description': error.description,
           }, error.code


@errors.app_errorhandler(IntegrityError)
def sqlalchemy_integrity_error(error):
    current_app.logger.error(f'400 Database integrity error {str(error.orig)}')
    return {
               'code': 400,
               'message': 'Database integrity error',
               'description': str(error.orig),
           }, 400


@errors.app_errorhandler(SQLAlchemyError)
def sqlalchemy_error(error):
    current_app.logger.error(
        f'{InternalServerError.code} Database error {str(error)}')
    if current_app.config['DEBUG'] is True:
        return {
                   'code': InternalServerError.code,
                   'message': 'Database error',
                   'description': str(error),
               }, 500
    else:
        return {
                   'code': InternalServerError.code,
                   'message': InternalServerError().name,
                   'description': InternalServerError.description,
               }, 500


@apifairy.error_handler
def validation_error(code, messages):
    current_app.logger.error(f'{code} Validation error(s): {messages}')
    return {
               'code': code,
               'message': 'Validation Error',
               'description': ('The server found one or more errors in the '
                               'information that you sent.'),
               'errors': messages,
           }, code
