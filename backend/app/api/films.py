from apifairy import response, body, authenticate, other_responses
from flask import Blueprint, abort, current_app
from flask_sqlalchemy import BaseQuery

from app import db
from app.api.auth import token_auth
from app.api.decorators import paginated_response
from app.api.schemas import FilmSchema
from app.models import Film

films = Blueprint('films', __name__)

film_schema = FilmSchema()
update_film_schema = FilmSchema(partial=True)
films_schema = FilmSchema(many=True)


@films.route('/films', methods=['POST'])
@authenticate(token_auth)
@body(film_schema)
@response(film_schema, 201)
def new(args) -> Film:
    """Submit a new film"""
    film = Film(user_id=token_auth.current_user().id, **args)
    db.session.add(film)
    db.session.commit()
    current_app.logger.info(f'Film {film.name} was added successfully')
    return film


@films.route('/films/<int:film_id>', methods=['GET'])
@response(film_schema)
def get(film_id: int) -> Film:
    """Get a film by id"""
    return Film.query.get(film_id) or abort(404)


@films.route('/films/search/<string:search_string>', methods=['GET'])
@paginated_response(films_schema)
def search(search_string: str) -> BaseQuery:
    """Search for films by name, with options to search and filter"""
    return Film.search(search_string)


@films.route('/films', methods=['GET'])
@paginated_response(films_schema)
def get_all() -> BaseQuery:
    """Retrieve list of all films, with options to search and filter"""
    return Film.query


@films.route('/films/<int:film_id>', methods=['PUT'])
@authenticate(token_auth)
@body(update_film_schema)
@response(film_schema)
@other_responses({403: 'Not allowed to edit this film, must be owner or admin',
                  404: 'Film not found'})
def put(data: Film, film_id: int) -> Film:
    """Edit a film"""
    film = db.session.get(Film, film_id) or abort(404)
    if film.user_id != token_auth.current_user().id and \
            not token_auth.current_user().is_admin:
        abort(403)
    film.update(data)
    db.session.commit()
    current_app.logger.info(f'Film {film.name} was updated successfully')
    return film


@films.route('/films/<int:film_id>', methods=['DELETE'])
@authenticate(token_auth)
@other_responses(
    {403: 'Not allowed to delete this film, must be owner or admin'})
def delete(film_id: int) -> (str, int):
    """Delete a film"""
    film = db.session.get(Film, film_id) or abort(404)
    if film.user_id != token_auth.current_user().id and \
            not token_auth.current_user().is_admin:
        abort(403)
    db.session.delete(film)
    db.session.commit()
    current_app.logger.info(f'Film {film.name} was deleted successfully')
    return '', 204
