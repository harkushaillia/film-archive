from apifairy import response
from flask import Blueprint, abort

from app.api.schemas import GenreSchema
from app.models import Genre

genres = Blueprint('genres', __name__)
genre_schema = GenreSchema()
genres_schema = GenreSchema(many=True)


@genres.route('/genres', methods=['GET'])
@response(genres_schema)
def get_all() -> list:
    """Get all genres"""
    return Genre.query.all()


@genres.route('/genres/<int:genre_id>', methods=['GET'])
@response(genre_schema)
def get(genre_id: int) -> Genre:
    """Get a genre by id"""
    return Genre.query.get(genre_id) or abort(404)
