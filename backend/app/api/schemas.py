from marshmallow import validate, validates, ValidationError, pre_dump, \
    post_dump, post_load

from app import ma, db
from app.models import User, Film, Director, Genre

paginated_schema_cache = {}


class PaginationSchema(ma.Schema):
    class Meta:
        ordered = True

    limit = ma.Integer(validate=validate.Range(min=1))
    page = ma.Integer(validate=validate.Range(min=1))
    year = ma.Integer()
    genre = ma.Integer()
    director = ma.Integer()
    sort_by = ma.String()
    count = ma.Integer(dump_only=True)
    total = ma.Integer(dump_only=True)

    @validates('sort_by')
    def validate_sort_by(self, value: str) -> None:
        valid_options = ('rating', 'release_date')
        if value not in valid_options:
            raise ValidationError(
                f'Not a valid sorting option, should be one of {valid_options}')


def paginated_collection(schema, pagination_schema=PaginationSchema):
    if schema in paginated_schema_cache:
        return paginated_schema_cache[schema]

    class PaginatedSchema(ma.Schema):
        class Meta:
            ordered = True

        pagination = ma.Nested(pagination_schema)
        data = ma.Nested(schema, many=True)

    PaginatedSchema.__name__ = f'Paginated{schema.__class__.__name__}'
    paginated_schema_cache[schema] = PaginatedSchema
    return PaginatedSchema


class UserSchema(ma.SQLAlchemySchema):
    class Meta:
        model = User
        ordered = True

    id = ma.auto_field(dump_only=True)
    username = ma.auto_field(required=True,
                             validate=validate.Length(min=3, max=64))
    is_admin = ma.auto_field(load_only=True, dump_default=False)
    email = ma.auto_field(required=True, validate=[validate.Length(max=120),
                                                   validate.Email()])
    password = ma.String(required=True, load_only=True,
                         validate=validate.Length(min=3))

    @validates('username')
    def validate_username(self, value: str) -> None:
        if not value[0].isalpha():
            raise ValidationError('Username must start with a letter')
        if User.query.filter_by(username=value).first() is not None:
            raise ValidationError('Use a different username.')


class DirectorSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Director

    id = ma.auto_field(dump_only=True)
    name = ma.auto_field(required=True,
                         validate=validate.Length(min=1, max=64))

    @validates('name')
    def validate_name(self, value: str) -> None:
        if Director.query.filter_by(name=value).first() is not None:
            raise ValidationError(
                'Director with that name is already registered')


class GenreSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Genre

    id = ma.auto_field(dump_only=True)
    name = ma.auto_field(required=True,
                         validate=validate.Length(min=1, max=64))


class FilmSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Film
        ordered = True

    id = ma.auto_field(dump_only=True)
    name = ma.auto_field(validate=validate.Length(min=1, max=120),
                         required=True)
    genres = ma.List(ma.Integer, load_only=True, required=True)
    genres_list = ma.Nested(GenreSchema(many=True), dump_only=True)
    release_date = ma.Date(required=True)
    director_id = ma.auto_field(required=True, load_only=True)
    director = ma.Nested(DirectorSchema, dump_only=True)
    description = ma.auto_field()
    rating = ma.auto_field(validate=validate.Range(min=0, max=10),
                           required=True)
    image_path = ma.Url(required=True)
    user_id = ma.auto_field(dump_only=True)
    user = ma.Nested(UserSchema, dump_only=True)

    @pre_dump
    def populate_genres(self, data: Film, **kwargs) -> Film:
        data.genres_list = data.genres
        return data

    @post_dump
    def set_unknown_director_if_none(self, data: Film, **kwargs) -> Film:
        if data['director'] is None:
            data['director'] = 'Unknown'
        return data

    @validates('director_id')
    def validate_genres(self, value: int, **kwargs) -> None:
        if db.session.query(Director.id).filter_by(id=value).first() is None:
            raise ValidationError(f'Director with id {value} was not found')

    @validates('genres')
    def validate_director(self, values: list, **kwargs) -> None:
        errors = {}
        for val in values:
            if db.session.query(Genre.id).filter_by(id=val).first() is None:
                errors[val] = [f'Genre with id {val} was not found']
        if errors:
            raise ValidationError(errors)

    @post_load
    def get_genres(self, data: Film, **kwargs) -> Film:
        if 'genres' in data:
            data['genres'] = [Genre.query.get(g_id) for g_id in data['genres']]
        return data


class TokenSchema(ma.Schema):
    class Meta:
        ordered = True

    access_token = ma.String(required=True)
