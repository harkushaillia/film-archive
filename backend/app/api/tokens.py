from apifairy import authenticate, response, other_responses
from flask import Blueprint

from app.api.auth import basic_auth
from app.api.schemas import TokenSchema

tokens = Blueprint('tokens', __name__)
token_schema = TokenSchema()


@tokens.route('/tokens', methods=['POST'])
@authenticate(basic_auth)
@response(token_schema)
@other_responses({401: 'Invalid username or password'})
def new():
    """Generate new access token"""
    access_token = basic_auth.current_user().generate_access_token()
    return {'access_token': access_token}
