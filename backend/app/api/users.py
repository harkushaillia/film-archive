from apifairy import body, response
from flask import Blueprint, current_app, abort

from app import db
from app.api.schemas import UserSchema
from app.models import User

users = Blueprint('users', __name__)
user_schema = UserSchema()


@users.route('/users', methods=['POST'])
@body(user_schema)
@response(user_schema, 201)
def new(args) -> User:
    """Register a new user"""
    user = User(**args)
    db.session.add(user)
    db.session.commit()
    current_app.logger.info(
        f'User {user.username} was registered successfully')
    return user


@users.route('/users/<int:user_id>', methods=['GET'])
@response(user_schema, 200)
def get(user_id: int) -> User:
    """Get the user by id"""
    return User.query.get(user_id) or abort(404)
