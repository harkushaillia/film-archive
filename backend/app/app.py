import logging
import os
from logging.handlers import RotatingFileHandler

from apifairy import APIFairy
from elasticsearch import Elasticsearch
from flask import Flask
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

from config import Config

db = SQLAlchemy()
ma = Marshmallow()
login = LoginManager()
login.login_view = 'auth.login'
bootstrap = Bootstrap()
apifairy = APIFairy()


def create_app(config_class=Config):
    template_folder = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                   'ui/templates')
    server = Flask(__name__, template_folder=template_folder)
    server.config.from_object(config_class)
    server.elasticsearch = Elasticsearch([server.config['ELASTICSEARCH_URL']]) \
        if server.config['ELASTICSEARCH_URL'] else None

    db.init_app(server)
    ma.init_app(server)
    login.init_app(server)
    bootstrap.init_app(server)
    apifairy.init_app(server)

    # ui blueprints
    from app.ui import ui_errors, ui_auth, ui_main
    server.register_blueprint(ui_errors)
    server.register_blueprint(ui_auth, url_prefix='/auth')
    server.register_blueprint(ui_main)
    # api blueprints
    from app.api import errors, users, directors, genres, tokens, films
    server.register_blueprint(errors)
    server.register_blueprint(users, url_prefix='/api')
    server.register_blueprint(directors, url_prefix='/api')
    server.register_blueprint(genres, url_prefix='/api')
    server.register_blueprint(tokens, url_prefix='/api')
    server.register_blueprint(films, url_prefix='/api')

    if not server.debug and not server.testing:
        logs_folder = server.config['LOGS_FOLDER']
        if not os.path.exists(logs_folder):
            os.mkdir(logs_folder)
        file_handler = RotatingFileHandler(
            os.path.join(logs_folder, 'film_archive.log'), maxBytes=10 ** 6,
            backupCount=10)
        file_handler.setFormatter(logging.Formatter(
            '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%('
            'lineno)d]'))
        file_handler.setLevel(logging.INFO)
        server.logger.addHandler(file_handler)
        server.logger.setLevel(logging.INFO)
        server.logger.info('Film archive startup')

    @server.shell_context_processor
    def make_shell_context() -> dict:
        ctx = {'db': db}
        # avoiding circular imports
        from app import models
        for attr in dir(models):
            model = getattr(models, attr)
            if hasattr(model, '__bases__') and \
                    db.Model in getattr(model, '__bases__'):
                ctx[attr] = model
        return ctx

    return server
