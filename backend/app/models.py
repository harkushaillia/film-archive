from __future__ import annotations

import secrets
from time import time

import jwt
from flask import current_app
from flask_login import UserMixin
from flask_sqlalchemy import BaseQuery, SignallingSession
from sqlalchemy import Column, ForeignKey, String, Boolean, Integer, Table, Date
from sqlalchemy.orm import relationship
from werkzeug.security import generate_password_hash, check_password_hash

from app import db, login
from app.search import add_to_index, remove_from_index, query_index


class Updatable:
    def update(self, data: dict) -> None:
        for attr, value in data.items():
            setattr(self, attr, value)


class SearchableMixin(object):
    @classmethod
    def search(cls: db.Model, expression: str = '') -> BaseQuery:
        if not expression:
            query = cls.query
            return query
        ids, total = query_index(cls.__tablename__, expression)
        if total == 0:
            return cls.query.filter_by(id=0)
        return cls.query.filter(cls.id.in_(ids))

    @classmethod
    def before_commit(cls: db.Model, session: SignallingSession) -> None:
        session._changes = {
            'add': list(session.new),
            'update': list(session.dirty),
            'delete': list(session.deleted)
        }

    @classmethod
    def after_commit(cls: db.Model, session: SignallingSession) -> None:
        for obj in session._changes['add']:
            if isinstance(obj, SearchableMixin):
                add_to_index(obj.__tablename__, obj)
        for obj in session._changes['update']:
            if isinstance(obj, SearchableMixin):
                add_to_index(obj.__tablename__, obj)
        for obj in session._changes['delete']:
            if isinstance(obj, SearchableMixin):
                remove_from_index(obj.__tablename__, obj)
        session._changes = None

    @classmethod
    def reindex(cls: db.Model):
        for obj in cls.query:
            add_to_index(cls.__tablename__, obj)


# adding listeners to enable indexing and searching
db.event.listen(db.session, 'before_commit', SearchableMixin.before_commit)
db.event.listen(db.session, 'after_commit', SearchableMixin.after_commit)

film_genre_association = Table(
    'film_genre_association', db.metadata,
    Column('film_id', Integer, db.ForeignKey('film.id'), primary_key=True),
    Column('genre_id', Integer, db.ForeignKey('genre.id'), primary_key=True))


class User(UserMixin, db.Model):
    id = Column(Integer, primary_key=True)
    username = Column(String(64), index=True, unique=True, nullable=False)
    is_admin = Column(Boolean, unique=False, nullable=False, default=False)
    email = Column(String(120), index=True, unique=True, nullable=False)
    password_hash = Column(String(128), nullable=False)
    added_films = relationship('Film', backref='user', lazy=True)

    def __repr__(self) -> str:
        return self.username

    @property
    def password(self) -> None:
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password: str) -> None:
        self.password_hash = generate_password_hash(password)

    def check_password(self, password: str) -> bool:
        return check_password_hash(self.password_hash, password)

    def generate_access_token(self) -> str:
        secret = secrets.token_urlsafe()
        access_token = self._generate_jwt(
            'access', current_app.config['ACCESS_TOKEN_EXPIRATION'],
            user_id=self.id, secret=secret)
        return access_token

    @staticmethod
    def check_access_token(access_token: str) -> User:
        data = User._verify_jwt('access', access_token)
        if data:
            user_id = data.get('user_id')
            user = db.session.get(User, user_id)
            if user:
                return user

    @staticmethod
    def _generate_jwt(token_type: str, expiration: float, **kwargs) -> str:
        return jwt.encode(
            {
                'type': token_type,
                'exp': time() + expiration,
                **kwargs
            },
            current_app.config['SECRET_KEY'],
            algorithm='HS256'
        )

    @staticmethod
    def _verify_jwt(token_type: str, token: str, verify_exp=True) -> \
            dict or None:
        try:
            data = jwt.decode(token, current_app.config['SECRET_KEY'],
                              algorithms=['HS256'],
                              options={'verify_exp': verify_exp})
        except jwt.PyJWTError:
            return
        if data.get('type') != token_type:
            return
        return data


class Film(SearchableMixin, db.Model, Updatable):
    __searchable__ = ['name']
    id = Column(Integer, primary_key=True)
    name = Column(String(64), index=True, unique=False, nullable=False)
    genres = relationship('Genre', secondary=film_genre_association,
                          backref=db.backref('film', lazy=True))
    release_date = Column(Date, index=True, nullable=False)
    director_id = Column(Integer, ForeignKey('director.id'), index=True,
                         nullable=True)
    description = Column(String)
    rating = Column(Integer, index=True, nullable=False)
    image_path = Column(String, nullable=False)
    user_id = Column(Integer, ForeignKey('user.id'))

    def __repr__(self) -> str:
        return '<Film {}>'.format(self.name)


class Genre(db.Model):
    id = Column(Integer, primary_key=True)
    name = Column(String(64), index=True, unique=True, nullable=False)

    def __repr__(self) -> str:
        return self.name


class Director(db.Model):
    id = Column(Integer, primary_key=True)
    name = Column(String(64), index=True, unique=True, nullable=False)
    films = relationship('Film', order_by='Film.name', backref='director')

    def __repr__(self) -> str:
        return self.name


@login.user_loader
def load_user(user_id: str) -> User:
    return User.query.get(int(user_id))
