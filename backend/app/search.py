from elasticsearch.exceptions import NotFoundError
from flask import current_app

from app import db


def add_to_index(index: str, model: db.Model) -> None:
    if not current_app.elasticsearch:
        return
    payload = {}
    for field in model.__searchable__:
        payload[field] = getattr(model, field)
    current_app.elasticsearch.index(index=index, doc_type=index, id=model.id,
                                    body=payload)


def remove_from_index(index: str, model: db.Model) -> None:
    if not current_app.elasticsearch:
        return
    try:
        current_app.elasticsearch.delete(index=index, doc_type=index,
                                         id=model.id)
    except NotFoundError:
        current_app.logger.error(
            f'Elasticsearch have not found an item with id {model.id}')


def query_index(index: str, query: str) -> (list, int):
    if not current_app.elasticsearch:
        current_app.logger.warn('App is running without search')
        return [], 0
    search_res = current_app.elasticsearch.search(index=index, doc_type=index,
                                                  body={
                                                      'query': {'multi_match': {
                                                          'query': query,
                                                          'fields': ['*']}}})
    ids = [int(hit['_id']) for hit in search_res['hits']['hits']]
    return ids, search_res['hits']['total']['value']
