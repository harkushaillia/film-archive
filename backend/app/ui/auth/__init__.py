from flask import Blueprint

ui_auth = Blueprint('auth', __name__)

from app.ui.auth import routes
