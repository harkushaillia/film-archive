from flask import url_for, flash, request, render_template, current_app
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.urls import url_parse
from werkzeug.utils import redirect

from app import db
from app.models import User
from app.ui.auth import ui_auth
from app.ui.auth.forms import LoginForm, RegistrationForm


@ui_auth.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('auth.login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('main.index')
        current_app.logger.info(f'User {current_user.username} logged in')
        return redirect(next_page)
    return render_template('auth/login.html', title='Sign In', form=form)


@ui_auth.route('/logout')
@login_required
def logout():
    current_app.logger.info(f'User {current_user.username} logged out')
    logout_user()
    return redirect(url_for('main.index'))


@ui_auth.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data,
                    password=form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Registration was successful')
        current_app.logger.info(
            f'User {form.username.data} was registered successfully')
        return redirect(url_for('auth.login'))
    return render_template('auth/register.html', title='Register', form=form)
