from flask import Blueprint

ui_errors = Blueprint('errors', __name__)

from app.ui.errors import handlers
