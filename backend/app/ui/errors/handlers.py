from flask import render_template, current_app

from app import db
from app.ui.errors import ui_errors


@ui_errors.errorhandler(401)
def unauthorized_error(error):
    current_app.logger.warning(f'Attempt of unauthorized access: {error}')
    return render_template('401.html'), 401


@ui_errors.errorhandler(404)
def not_found_error(error):
    current_app.logger.error(error)
    return render_template('404.html'), 404


@ui_errors.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    current_app.logger.error(error)
    return render_template('500.html'), 500
