from flask import Blueprint

ui_main = Blueprint('main', __name__)

from app.ui.main import routes
