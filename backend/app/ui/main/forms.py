from flask import request
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, \
    IntegerRangeField, DateField, \
    TextAreaField, SelectField, URLField
from wtforms.validators import DataRequired, ValidationError, \
    Optional, InputRequired, Length
from wtforms.widgets import NumberInput
from wtforms_sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField

from app.models import Director, Genre, Film
from app.ui.constants import SORTING_OPTIONS


class AddFilmForm(FlaskForm):
    name = StringField('Film name',
                       validators=[InputRequired(), Length(max=255)])

    genres = QuerySelectMultipleField(
        'Genres', query_factory=lambda: Genre.query.all(),
        validators=[InputRequired()])

    release_date = DateField(
        'Release date', format='%Y-%m-%d', validators=[InputRequired()])

    director = QuerySelectField(
        'Director', query_factory=lambda: Director.query.all(),
        validators=[InputRequired()])

    description = TextAreaField(
        'Description', validators=[Optional(), Length(max=5000)])

    rating = IntegerRangeField(
        'Rating', widget=NumberInput(min=0, max=10, step=1),
        validators=[InputRequired()])

    poster = URLField('Poster', validators=[InputRequired()])

    submit = SubmitField('Add film')


class EditFilmForm(FlaskForm):
    name = StringField(
        'Film name', validators=[DataRequired(), Length(max=255)])

    genres = QuerySelectMultipleField(
        'Genres', query_factory=lambda: Genre.query.all(),
        validators=[InputRequired()])

    release_date = DateField(
        'Release date', format='%Y-%m-%d', validators=[InputRequired()])

    director = QuerySelectField(
        'Director', query_factory=lambda: Director.query.all(),
        validators=[InputRequired()])

    description = TextAreaField(
        'Description', validators=[Optional(), Length(max=5000)])

    rating = IntegerRangeField(
        'Rating', widget=NumberInput(min=0, max=10, step=1),
        validators=[InputRequired()])

    poster = URLField('Poster')

    submit = SubmitField('Edit film')


class AddDirectorForm(FlaskForm):
    director_name = StringField(
        'Director name', validators=[DataRequired(), Length(max=255)])

    submit = SubmitField('Add')

    def validate_director_name(self, director_name: StringField) -> None:
        name = Director.query.filter_by(name=director_name.data).first()
        if name is not None:
            raise ValidationError(
                'A director with the same name has already been added')


class RemoveDirectorForm(FlaskForm):
    director = QuerySelectField(
        'Director', query_factory=lambda: Director.query.all(),
        validators=[InputRequired()])

    submit = SubmitField('Remove')


class SearchForm(FlaskForm):
    q = StringField('Search by title', validators=[Length(max=255)])
    genre = QuerySelectField(
        'Genre', query_factory=lambda: Genre.query.all(), allow_blank=True,
        blank_text='Any', validators=[InputRequired()])
    director = QuerySelectField(
        'Director', query_factory=lambda: Director.query.all(), default=None,
        allow_blank=True, blank_text='Any', validators=[InputRequired()])
    year = QuerySelectField(
        'Year', get_pk=lambda x: x, allow_blank=True, blank_text="Any",
        query_factory=lambda: {film.release_date.year for film in
                               Film.query.all()}, default=None,
        validators=[InputRequired()])
    order_by = SelectField(
        'Order by', choices=SORTING_OPTIONS, default=SORTING_OPTIONS[0][0],
        validators=[InputRequired()])

    def __init__(self, *args, **kwargs):
        if 'formdata' not in kwargs:
            kwargs['formdata'] = request.args
        if 'csrf_enabled' not in kwargs:
            kwargs['csrt_enabled'] = False
        super(SearchForm, self).__init__(*args, **kwargs)
