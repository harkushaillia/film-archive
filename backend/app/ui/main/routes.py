from flask import render_template, flash, redirect, url_for, request, \
    send_from_directory, current_app
from flask_login import current_user, login_required
from sqlalchemy import extract

from app import db
from app.ui.main import ui_main
from app.ui.main.forms import AddFilmForm, AddDirectorForm, RemoveDirectorForm, \
    EditFilmForm, SearchForm
from app.models import User, Director, Film, Genre


@ui_main.before_app_first_request
def db_setup():
    db.create_all()
    if User.query.first() is None:
        db.session.add(User(username='admin', email='some@mail.com',
                            is_admin=True, password='admin'))
        db.session.commit()
        current_app.logger.info('No users detected - created default admin')
    if Genre.query.first() is None:
        for i in range(1, 4):
            db.session.add(Genre(name=f'Genre {i}'))
        db.session.commit()
        current_app.logger.info('No genres detected - created 3 default')
    if Director.query.first() is None:
        for i in range(1, 4):
            db.session.add(Director(name=f'Director {i}'))
        db.session.commit()
        current_app.logger.info('No directors detected - created 3 default')


@ui_main.route('/')
@ui_main.route('/index')
def index():
    s_form = SearchForm()
    page_size = current_app.config['ITEMS_PER_PAGE']
    page = request.args.get('page', 1, type=int)

    f_query = Film.search(s_form.q.data) if s_form.q.data else Film.query
    if genre := s_form.genre.data:
        f_query = f_query.filter(Film.genres.any(id=genre.id))
    if director := s_form.director.data:
        f_query = f_query.filter_by(director=director)
    if year := s_form.year.data:
        f_query = f_query.filter(extract('year', Film.release_date) == year)
    films = f_query.order_by(
        getattr(Film, s_form.order_by.data).desc()).paginate(
        page, page_size, False)

    next_url = url_for(
        'main.index',
        **{**request.args, 'page': films.next_num}) if films.has_next else None
    prev_url = url_for(
        'main.index',
        **{**request.args, 'page': films.prev_num}) if films.has_prev else None
    username = current_user.username if current_user.is_authenticated else "anonymous"
    current_app.logger.info(f'Index opened by {username}')
    return render_template(
        'index.html', title='Home', films=films.items, next_url=next_url,
        prev_url=prev_url, form=s_form)


@ui_main.route('/add_film', methods=['GET', 'POST'])
@login_required
def add_film():
    form = AddFilmForm()

    if form.validate_on_submit():
        user = User.query.filter_by(id=current_user.id).first()
        film = Film(name=form.name.data,
                    genres=form.genres.data,
                    release_date=form.release_date.data,
                    director=form.director.data,
                    description=form.description.data,
                    rating=form.rating.data,
                    # image_path=f_name,
                    image_path=form.poster.data,
                    user=user)
        db.session.add(film)
        db.session.commit()
        flash(f'Film {form.name.data} was added successfully')
        current_app.logger.info(f'Film {form.name.data} was added successfully')
        return redirect(url_for('main.index'))

    return render_template('add_film.html', title='Add Film', form=form)


@ui_main.route('/edit_film/<film_id>', methods=['GET', 'POST'])
@login_required
def edit_film(film_id):
    film = Film.query.filter_by(id=film_id).first_or_404()
    if current_user.id != film.user_id and not current_user.is_admin:
        return render_template('401.html')
    form = EditFilmForm(obj=film, genres=film.genres)

    if form.validate_on_submit():
        film.image_path = form.poster.data
        film.name = form.name.data
        film.genres = form.genres.data
        film.release_date = form.release_date.data
        film.director = form.director.data
        film.description = form.description.data
        film.rating = form.rating.data
        db.session.add(film)
        db.session.commit()
        flash(f'Film {form.name.data} was updated successfully')
        current_app.logger.info(
            f'Film {form.name.data} was updated successfully')
        return redirect(url_for('main.index'))

    return render_template('edit_film.html',
                           current_poster=film.image_path,
                           title='Edit film',
                           form=form)


@ui_main.route('/delete_film/<film_id>')
@login_required
def delete_film(film_id):
    film = Film.query.filter_by(id=film_id).first_or_404()
    if current_user.id != film.user_id and not current_user.is_admin:
        return render_template('401.html')
    db.session.delete(film)
    db.session.commit()
    flash('Film {} was deleted'.format(film.name))
    current_app.logger.info(f'Film {film.name} was deleted successfully')
    return redirect(url_for('main.index'))


@ui_main.route('/add_director', methods=['GET', 'POST'])
@login_required
def add_director():
    form = AddDirectorForm()
    if form.validate_on_submit():
        director = Director(name=form.director_name.data)
        db.session.add(director)
        db.session.commit()
        flash(f'{form.director_name.data} was added')
        current_app.logger.info(
            f'Director {director.name} was added successfully')
        return redirect(url_for('main.index'))
    return render_template('add_director.html', title='Add Director', form=form)


@ui_main.route('/remove_director', methods=['GET', 'POST'])
@login_required
def remove_director():
    form = RemoveDirectorForm()
    if form.validate_on_submit():
        director = Director.query.filter_by(id=form.director.data.id).first()
        db.session.delete(director)
        db.session.commit()
        flash(f'Director {form.director.data.name} was removed')
        current_app.logger.info(
            f'Director {form.director.data.name} was removed')
        return redirect(url_for('main.index'))
    return render_template('remove_director.html', title='Remove Director',
                           form=form)
