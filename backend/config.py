import os

from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '../.env'))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'sword-fish'
    ACCESS_TOKEN_EXPIRATION = int(os.environ.get(
        'ACCESS_TOKEN_EXPIRATION', '60')) * 60  # 1 hour
    DISABLE_AUTH = bool(os.environ.get('DISABLE_AUTH')) or False
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
                              'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    ITEMS_PER_PAGE = 10
    ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL') or None
    LOGS_FOLDER = os.environ.get('LOGS_FOLDER') or '/logs'

    # supported documentation formats are
    # “redoc”, “swagger_ui”, “rapidoc” and “elements”
    APIFAIRY_UI = os.environ.get('DOCS_UI', 'elements')
    APIFAIRY_TITLE = 'Film Archive API'
    APIFAIRY_VERSION = '1.0'
