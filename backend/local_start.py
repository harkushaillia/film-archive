from app import create_app
from local_config import Config

server = create_app(Config)

if __name__ == '__main__':
    server.run(debug=True, host='0.0.0.0')
