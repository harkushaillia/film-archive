import unittest

from app import create_app, db
from app.models import User, Genre, Director
from config import Config


class TestConfig(Config):
    SERVER_NAME = 'localhost:5000'
    TESTING = True
    DISABLE_AUTH = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    ELASTICSEARCH_URL = None


class TestConfigWithAuth(TestConfig):
    DISABLE_AUTH = False


class BaseTest(unittest.TestCase):
    config = TestConfig

    def setUp(self) -> None:
        self.app = create_app(self.config)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        db.session.add(User(username='admin', is_admin=True,
                            email='test@example.com', password='spam'))
        db.session.add(Genre(name='Fantasy'))
        db.session.add(Genre(name='Comedy'))
        db.session.add(Director(name='Gilliam'))
        db.session.add(Director(name='MacNaughton'))
        db.session.commit()
        self.client = self.app.test_client()

    def tearDown(self) -> None:
        db.session.remove()
        db.drop_all()
        self.app_context.pop()
