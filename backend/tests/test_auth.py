from app import db
from app.models import User
from tests.base_test import BaseTest, TestConfigWithAuth


class AuthTests(BaseTest):
    config = TestConfigWithAuth

    def test_no_auth(self) -> None:
        # films list is accessible without authorisation
        rv = self.client.get('/api/films')
        assert rv.status_code == 200
        assert rv.json['pagination']['total'] == 0

        # unable to post a film without authorisation
        rv = self.client.post('/api/films', json={
            'name': 'Monty Python and the Holy Grail',
            'genres': [1],
            'release_date': '2019-08-24',
            'director_id': 1,
            'description': 'This is a test film',
            'rating': 1,
            'image_path': 'http://www.example.com'
        })
        assert rv.status_code == 401

        # unable to edit a film without authorisation
        rv = self.client.put('/api/films/1', json={
            'name': 'Monty Python and the Holy Grail'
        })
        assert rv.status_code == 401

        # unable to delete a film without authorisation
        rv = self.client.delete('/api/films/1')
        assert rv.status_code == 401

    def test_auth(self) -> None:
        u = User(username='non_admin', email='some@mail.com', password='pass')
        db.session.add(u)
        db.session.commit()
        rv = self.client.post('/api/tokens', auth=('non_admin', 'pass'))
        assert rv.status_code == 200
        access_token = rv.json['access_token']

        # authorised user can create film
        rv = self.client.post('/api/films', headers={
            'Authorization': f'Bearer {access_token}'}, json={
            'name': 'Monty Python and the Holy Grail',
            'genres': [1],
            'release_date': '2019-08-24',
            'director_id': 1,
            'description': 'This is a test film',
            'rating': 1,
            'image_path': 'http://www.example.com'
        })
        assert rv.status_code == 201
        assert rv.json['name'] == 'Monty Python and the Holy Grail'
        id = rv.json['id']

        # authorized user can get films list
        rv = self.client.get('/api/films')
        assert rv.status_code == 200
        assert rv.json['pagination']['total'] == 1

        # authorized user can edit his film
        rv = self.client.put(f'/api/films/{id}', headers={
            'Authorization': f'Bearer {access_token}'}, json={
            'name': 'And Now for Something Completely Different'
        })
        assert rv.status_code == 200
        assert rv.json['name'] == 'And Now for Something Completely Different'

        # authorized user can delete his film
        rv = self.client.delete(f'/api/films/{id}', headers={
            'Authorization': f'Bearer {access_token}'
        })
        assert rv.status_code == 204

    def test_film_ownership(self) -> None:
        u = User(username='non_admin', email='some@mail.com', password='pass')
        db.session.add(u)
        db.session.commit()
        rv = self.client.post('/api/tokens', auth=('non_admin', 'pass'))
        assert rv.status_code == 200
        non_admin_token = rv.json['access_token']

        rv = self.client.post('/api/tokens', auth=('admin', 'spam'))
        assert rv.status_code == 200
        admin_token = rv.json['access_token']

        rv = self.client.post('/api/films', headers={
            'Authorization': f'Bearer {admin_token}'}, json={
            'name': 'Admin film',
            'genres': [1],
            'release_date': '2019-08-24',
            'director_id': 1,
            'rating': 1,
            'image_path': 'http://www.example.com'
        })
        assert rv.status_code == 201
        admin_film_id = rv.json['id']

        rv = self.client.post('/api/films', headers={
            'Authorization': f'Bearer {non_admin_token}'}, json={
            'name': 'Non-admin film',
            'genres': [1],
            'release_date': '2019-08-24',
            'director_id': 1,
            'rating': 1,
            'image_path': 'http://www.example.com'
        })
        assert rv.status_code == 201
        non_admin_film_id = rv.json['id']

        # user can not edit a film that he has not added
        rv = self.client.put(f'/api/films/{admin_film_id}', headers={
            'Authorization': f'Bearer {non_admin_token}'}, json={
            'name': 'And Now for Something Completely Different'
        })
        assert rv.status_code == 403
        assert rv.json['message'] == 'Forbidden'
        assert self.client.get(f'/api/films/{admin_film_id}').json['name'] == \
               'Admin film'

        # user can not delete a film that he has not added
        rv = self.client.delete(f'/api/films/{admin_film_id}', headers={
            'Authorization': f'Bearer {non_admin_token}'
        })
        assert rv.status_code == 403
        assert rv.json['message'] == 'Forbidden'
        assert self.client.get(f'/api/films/{admin_film_id}').json['name'] == \
               'Admin film'

        # admin can edit a film that he has not added
        rv = self.client.put(f'/api/films/{non_admin_film_id}', headers={
            'Authorization': f'Bearer {admin_token}'}, json={
            'name': 'And Now for Something Completely Different'
        })
        assert rv.status_code == 200
        assert rv.json['name'] == 'And Now for Something Completely Different'
        assert self.client.get(f'/api/films/{non_admin_film_id}').json['name'] \
               == 'And Now for Something Completely Different'

        # admin can delete a film that he has not added
        rv = self.client.delete(f'/api/films/{non_admin_film_id}', headers={
            'Authorization': f'Bearer {admin_token}'
        })
        assert rv.status_code == 204
        assert self.client.get(f'/api/films/{non_admin_film_id}').status_code \
               == 404
