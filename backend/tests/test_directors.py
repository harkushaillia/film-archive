from tests.base_test import BaseTest


class DirectorsTests(BaseTest):
    def test_new_director(self) -> None:
        # new directors can be created
        rv = self.client.post('/api/directors', json={
            'name': 'Jones'
        })
        assert rv.status_code == 201
        assert rv.json['name'] == 'Jones'
        id = rv.json['id']

        # directors can be got by id
        rv = self.client.get(f'/api/directors/{id}')
        assert rv.status_code == 200
        assert rv.json['name'] == 'Jones'

    def test_delete_director(self) -> None:
        rv = self.client.post('/api/directors', json={
            'name': 'Jones'
        })
        assert rv.status_code == 201
        director_id = rv.json['id']

        rv = self.client.post('/api/films', json={
            'name': "Monty Python's The Meaning of Life",
            'genres': [1],
            'release_date': '1983-06-23',
            'director_id': director_id,
            'rating': 1,
            'image_path': 'http://www.example.com'
        })
        assert rv.status_code == 201
        assert rv.json['director']['name'] == 'Jones'
        film_id = rv.json['id']

        # directors can be deleted by id
        rv = self.client.delete(f'/api/directors/{director_id}')
        assert rv.status_code == 204

        # if director is deleted,
        # his films will return 'Unknown' in director field
        rv = self.client.get(f'/api/films/{film_id}')
        assert rv.status_code == 200
        assert rv.json['director'] == 'Unknown'
        assert rv.json['name'] == "Monty Python's The Meaning of Life"
