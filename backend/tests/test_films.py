from tests.base_test import BaseTest


class FilmTests(BaseTest):
    def test_new_film(self) -> None:
        # film can be created with valid data
        rv = self.client.post('/api/films', json={
            'name': 'Monty Python and the Holy Grail',
            'genres': [1],
            'release_date': '2019-08-24',
            'director_id': 1,
            'description': 'This is a test film',
            'rating': 1,
            'image_path': 'http://www.example.com'
        })
        assert rv.status_code == 201
        assert rv.json['name'] == 'Monty Python and the Holy Grail'
        assert rv.json['genres_list'][0]['name'] == 'Fantasy'
        assert rv.json['release_date'] == '2019-08-24'
        assert rv.json['director']['name'] == 'Gilliam'
        assert rv.json['description'] == 'This is a test film'
        assert rv.json['rating'] == 1
        assert rv.json['image_path'] == 'http://www.example.com'
        assert rv.json['user_id'] == 1
        id = rv.json['id']

        # can get new film
        rv = self.client.get(f'/api/films/{id}')
        assert rv.status_code == 200
        assert rv.json['name'] == 'Monty Python and the Holy Grail'

        # new film is present in the list
        rv = self.client.get(f'/api/films')
        assert rv.status_code == 200
        assert rv.json['pagination']['total'] == 1
        assert rv.json['data'][0]['name'] == 'Monty Python and the Holy Grail'

        # film can not be created with invalid data
        rv = self.client.post(f'/api/films', json={
            'name': 12,
            'genres': 1,
            'release_date': 1971,
            'director_id': 'Gilliam',
            'rating': 'ten',
            'image_path': '/my_images/poster.jpg'
        })
        assert rv.status_code == 400
        assert rv.json['message'] == 'Validation Error'
        assert rv.json['description'] == 'The server found one or more errors ' \
                                         'in the information that you sent.'
        re = rv.json['errors']['json']
        assert re['name'] == ['Not a valid string.']
        assert re['genres'] == ['Not a valid list.']
        assert re['release_date'] == ['Not a valid date.']
        assert re['director_id'] == ['Not a valid integer.']
        assert re['rating'] == ['Not a valid integer.']
        assert re['image_path'] == ['Not a valid URL.']

    def test_edit_film(self) -> None:
        rv = self.client.post('/api/films', json={
            'name': 'Monty Python and the Holy Grail',
            'genres': [1],
            'release_date': '2019-08-24',
            'director_id': 1,
            'description': 'This is a test film',
            'rating': 10,
            'image_path': 'http://www.example.com'
        })
        assert rv.status_code == 201
        id = rv.json['id']

        # film can be edited with valid data
        rv = self.client.put(f'/api/films/{id}', json={
            'name': 'And Now for Something Completely Different',
            'genres': [2],
            'release_date': '1971-09-28',
            'director_id': 2,
            'description': 'Something completely different',
            'rating': 9,
            'image_path': 'http://www.example2.com'
        })
        assert rv.status_code == 200
        assert rv.json['description'] == 'Something completely different'
        assert rv.json['name'] == 'And Now for Something Completely Different'
        assert rv.json['genres_list'][0]['name'] == 'Comedy'
        assert rv.json['release_date'] == '1971-09-28'
        assert rv.json['director']['name'] == 'MacNaughton'
        assert rv.json['description'] == 'Something completely different'
        assert rv.json['rating'] == 9
        assert rv.json['image_path'] == 'http://www.example2.com'

        # film can not be edited with invalid data
        rv = self.client.put(f'/api/films/{id}', json={
            'name': '',
            'genres': [3, 1],
            'release_date': '1971-09-32',
            'director_id': 3,
            'rating': 11,
            'image_path': 'just text'
        })
        assert rv.status_code == 400
        assert rv.json['message'] == 'Validation Error'
        assert rv.json['description'] == 'The server found one or more errors ' \
                                         'in the information that you sent.'
        re = rv.json['errors']['json']
        assert re['name'] == ['Length must be between 1 and 120.']
        assert re['genres'] == {'3': ['Genre with id 3 was not found']}
        assert re['release_date'] == ['Not a valid date.']
        assert re['director_id'] == ['Director with id 3 was not found']
        assert re['rating'] == ['Must be greater than or equal to 0 and less '
                                'than or equal to 10.']
        assert re['image_path'] == ['Not a valid URL.']

    def test_delete_film(self) -> None:
        rv = self.client.post('/api/films', json={
            'name': 'Monty Python and the Holy Grail',
            'genres': [1],
            'release_date': '2019-08-24',
            'director_id': 1,
            'rating': 1,
            'image_path': 'http://www.example.com'
        })
        assert rv.status_code == 201
        id = rv.json['id']

        # film can be deleted
        rv = self.client.delete(f'/api/films/{id}')
        assert rv.status_code == 204

        # unable to get deleted film
        rv = self.client.get(f'/api/films/{id}')
        assert rv.status_code == 404

        # film is no longer present on the list
        rv = self.client.get('/api/films')
        assert rv.status_code == 200
        assert rv.json['pagination']['total'] == 0

    def test_sort_and_filter_films(self) -> None:
        assert self.client.post('/api/films', json={
            'name': 'Mediocre mid-aged film',
            'genres': [1, 2],
            'release_date': '2005-08-24',
            'director_id': 2,
            'rating': 5,
            'image_path': 'http://www.example.com'
        }).status_code == 201

        assert self.client.post('/api/films', json={
            'name': 'Best old film',
            'genres': [1],
            'release_date': '2000-08-24',
            'director_id': 1,
            'rating': 10,
            'image_path': 'http://www.example2.com'
        }).status_code == 201

        assert self.client.post('/api/films', json={
            'name': 'Worst new film',
            'genres': [2],
            'release_date': '2020-08-24',
            'director_id': 2,
            'rating': 0,
            'image_path': 'http://www.example3.com'
        }).status_code == 201

        # sorting by release date will return the oldest film first
        rv = self.client.get('/api/films?sort_by=release_date')
        assert rv.status_code == 200
        assert rv.json['data'][0]['name'] == 'Best old film'

        # sorting by rating will return the lowest rated film first
        rv = self.client.get('/api/films?sort_by=rating')
        assert rv.status_code == 200
        assert rv.json['data'][0]['name'] == 'Worst new film'

        # sorting is only possible by release date and rating
        rv = self.client.get('/api/films?sort_by=name')
        assert rv.status_code == 400
        assert rv.json['message'] == 'Validation Error'
        assert rv.json['errors']['query']['sort_by'] == [
            "Not a valid sorting option, should be one of ('rating', "
            "'release_date')"]

        # filtering by genre will return only films that belong to it
        rv = self.client.get('/api/films?genre=1')
        assert rv.status_code == 200
        assert rv.json['pagination']['total'] == 2
        for film in rv.json['data']:
            assert 1 in (genre['id'] for genre in film['genres_list'])

        # filtering by director will return only films with that director
        rv = self.client.get('/api/films?director=2')
        assert rv.status_code == 200
        assert rv.json['pagination']['total'] == 2
        for film in rv.json['data']:
            assert film['director']['id'] == 2
            assert film['director']['name'] == 'MacNaughton'

        # filtering by year will return only films released that year
        rv = self.client.get('/api/films?year=2000')
        assert rv.status_code == 200
        assert rv.json['pagination']['total'] == 1
        assert '2000' in rv.json['data'][0]['release_date']

        # filters can be applied simultaneously
        rv = self.client.get('/api/films?year=2020&genre=2&director=2')
        assert rv.status_code == 200
        assert rv.json['pagination']['total'] == 1
        assert '2020' in rv.json['data'][0]['release_date']
        assert rv.json['data'][0]['director']['id'] == 2
        assert rv.json['data'][0]['director']['id'] == 2
        assert 2 in (genre['id'] for genre in rv.json['data'][0]['genres_list'])

        # empty list is returned if there are no matching films
        rv = self.client.get('/api/films?year=3000')
        assert rv.status_code == 200
        assert rv.json['pagination']['total'] == 0
        assert not rv.json['data']

    def test_films_pagination(self) -> None:
        for i in range(12):
            assert self.client.post('/api/films', json={
                'name': f'Test film number {i}',
                'genres': [1],
                'release_date': '2005-08-24',
                'director_id': 1,
                'rating': 5,
                'image_path': 'http://www.example.com'
            }).status_code == 201

        # default page size is 10 and page number is 1
        rv = self.client.get('/api/films')
        assert rv.status_code == 200
        assert rv.json['pagination']['count'] == 10
        assert rv.json['pagination']['limit'] == 10
        assert rv.json['pagination']['page'] == 1
        assert rv.json['pagination']['total'] == 12

        # page size and number can be set as query parameters
        rv = self.client.get('/api/films?limit=5&page=3')
        assert rv.status_code == 200
        assert rv.json['pagination']['count'] == 2
        assert rv.json['pagination']['limit'] == 5
        assert rv.json['pagination']['page'] == 3
        assert rv.json['pagination']['total'] == 12

        # minimal value for page and limit is 1
        rv = self.client.get('/api/films?limit=0&page=0')
        assert rv.status_code == 400
        assert rv.json['message'] == 'Validation Error'
        assert rv.json['errors']['query']['limit'] == [
            "Must be greater than or equal to 1."]
        assert rv.json['errors']['query']['page'] == [
            "Must be greater than or equal to 1."]

        # attempt to get page with number higher than total/limit will return
        # 404 Not Found error
        rv = self.client.get('/api/films?page=3')
        assert rv.status_code == 404
        assert rv.json['message'] == 'Not Found'
