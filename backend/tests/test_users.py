from app.models import User
from tests.base_test import BaseTest


class UserTest(BaseTest):

    def test_password_hashing(self) -> None:
        u = User(username='susan', password='cat')
        # password is verified securely
        self.assertFalse(u.check_password('dog'))
        self.assertTrue(u.check_password('cat'))

    def test_user_creating(self) -> None:
        # new users can be created
        rv = self.client.post('/api/users', json={
            'username': 'King Arthur',
            'email': 'spam@eggs.com',
            'password': 'Britain'
        })
        assert rv.status_code == 201
        assert rv.json['username'] == 'King Arthur'
        assert rv.json['email'] == 'spam@eggs.com'
        assert 'password' not in rv.json
        id = rv.json['id']

        # users can be got by id
        rv = self.client.get(f'/api/users/{id}')
        assert rv.status_code == 200
        assert rv.json['username'] == 'King Arthur'
        assert rv.json['email'] == 'spam@eggs.com'
